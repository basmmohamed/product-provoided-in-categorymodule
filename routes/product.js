const express = require("express");
const router = express.Router();
const productController = require("../controllers/productController")
// const multer = require('multer');
// const path = require('path');
// const upload = multer({
//     dest: path.resolve('../Server/public/upload/products'),
//     limits: { fileSize: 2000000, files: 1 }, // 2M File
// });

router.post("/", productController.addOneProduct);
router.post("/productprice", productController.addOneProductByProvoider);
router.get("/",productController.getAllProduct)
router.get("/productprice",productController.getAllProductProvoiderPrice)

module.exports = router;
