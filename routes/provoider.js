const express = require("express");
const router = express.Router();
const provoiderController = require("../controllers/provoiderController");


router.post("/", provoiderController.addOneProvoider);
router.get("/", provoiderController.getAllProvoider);

module.exports = router;
