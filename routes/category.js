const express = require("express");
const router = express.Router();
const categoryController = require("../controllers/categoryController");
// const multer = require('multer');
// const path = require('path');
const { pagination } = require("../middlewares/pagination");

router.patch("/category/:productid", categoryController.featureOneProduct);

router.post("/category/", categoryController.addOneCategory);
router.get(
  "/category/:categoryid",
  [pagination],
  categoryController.getAllProductLowProvoiderPrice
);
router.get("/category/", categoryController.getAllCategory);

// router.get("/category/:categoryid", categoryController.getAllProductInCategory);

module.exports = router;
