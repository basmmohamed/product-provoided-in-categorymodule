const mongoose = require('mongoose');
mongoose.Promise = global.Promise;

const db = {};

db.mongoose = mongoose;

db.productModel = require("./product");
db.categoryModel=require("./category");
db.provoiderModel=require("./provoider");
db.productprovoiderModel=require("./productprovoider")
module.exports = db;