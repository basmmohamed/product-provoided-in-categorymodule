const mongoose= require('mongoose');
const productSchema=mongoose.Schema({
    name : {type:String,required:true,maxlength: 45},
    image_uri : {type:String,required:true, maxlength:255},
    category_id:{type: mongoose.Schema.Types.ObjectId, ref: 'Category'},
    featured :{type:Boolean, default:false},
})

module.exports=mongoose.model("Product",productSchema)
