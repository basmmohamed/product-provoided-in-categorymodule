const mongoose=require("mongoose");
const productprovoiderSchema=mongoose.Schema({
    product_id: {type: mongoose.Schema.Types.ObjectId, ref: 'Product',required:true},
    provoider_id: {type: mongoose.Schema.Types.ObjectId, ref: 'Provoider',required:true},
    price:{type:Number,required:true,min:0},
    available:{type:Boolean, required:true},
    productcategory_id:{type: mongoose.Schema.Types.ObjectId, ref: 'Category',required:true}
});
module.exports=mongoose.model("ProductProvoider",productprovoiderSchema);