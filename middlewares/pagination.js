exports.pagination = async function (req, res, next) {
  let page = parseInt(req.query.page); // Make sure to parse the page to number
  const limit = 2;
  let skip = (page - 1) * limit;

  if (page <= 0) {
    req.query.page = 1;
    page=1;
    skip = 0;
  }
  res.skip = skip;
  res.limit = limit;
  res.page = page;

  next();
};
