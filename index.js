const mongoose = require("mongoose");
const express = require("express");
const app = express();
const PORT = process.env.PORT || 5000;
const DB_HOST = process.env.DB_HOST;
const DB_DATABASE = process.env.DB_DATABASE;
const DB_PORT = process.env.DB_PORT;

const categoryRouter = require("./routes/category.js");
const productRouter = require("./routes/product");
const provoiderRouter = require("./routes/provoider");
//Database connection
mongoose.set("useCreateIndex", true);
mongoose.connect(
  `mongodb://${DB_HOST}:${DB_PORT}/${DB_DATABASE}`,
  {
    useNewUrlParser: true,
    useUnifiedTopology: true,
    useFindAndModify: false,
  },
  (err) => {
    if (!err) {
      console.log("Started connection to mongo");
    } else console.log(err);
  }
);

app.use(express.json());//middleware

//Server POrt listening
app.listen(PORT, (err) => {
  if (!err) console.log(`Server Connected on port ${PORT}`);
  else console.log("Couldnot  Connect to mentioned port");
});

app.use("/", categoryRouter);
app.use("/product", productRouter);
app.use("/provoider", provoiderRouter);
