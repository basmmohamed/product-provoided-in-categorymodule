# Product Provoider BY Category Module
A manufacturer produces Products of different categories  and supplied by different prizes grouped by categories . The implemented system keeps track of the produced products within categories each product  can be featured or not. It is meant to be as a mock for a multi-purpose platform for developing and learning issues:
1. Implement a database schema. 
2. Implement two API endpoints for the basic toggle and read functionalities for the products within category using product id.
     A user can toggle a product. patch request on http://localhost:5000/category/:productid/
     A user can list all the products ordered with lowest price produced so far showing each product’s attributes using category id .get request on http://localhost:5000/category/:categoryid?page=1
3. Implement Another middleware for pagination as max results/page is 2  products 

[source code  on gitlab](https://gitlab.com/basmmohamed/product-provoided-in-categorymodule)

## Getting Started

These steps will get you a copy of the project up and running for development and testing purposes.

## Installation

1.  Install  **Nodejs** _latest stable version_
2.  Install  **npm** _latest stable version_
3.  Install  **mongoDB v3.6.3**
4. _Optional Step_ ⇒ You can install **MongoDB Compass** [any user interface application for MongoDB] as it offers a user interface for dealing with the database
5.  Clone the Project
6. 
	Run the following commands:
	```
	npm install
	cp .env.example .env
	
7.  Create an empty database with named  **productProvoiderDB**  in mongoDB [You can do so easily using MongoDB Compass or any user interface application for MongoDB] or in shell by writing **mongo** press enter to open shell then **use productProvoiderDB** to create empty database.
8.  In the **.env** file change the  `DB_HOST`,  `DB_DATABASE`, `DB_USERNAME`, `DB_PASSWORD` and `DB_PORT`  variables to match the credentials of the database you just created. 
_You can optionally change other variables like `PORT` which is the port the Server is running on, but if there's no need for so we prefer to keep them as they are_	```

## Usage

1.  run the following command to launch the project:  `npm start`

## Server Features

1. Database schema for product, provoider, category, products'provoider' models.
2. Two main API endpoints for toggle and read functionalities for the batch.
3. User can feature a product.
4. User can list all products.
5. Another middleware for pagination between results.

## Built With

[ Expressjs ](https://expressjs.com/), [Nodejs](https://nodejs.org/en/), [MongoDB](https://www.mongodb.com/) and [Postman](https://www.postman.com/)

## Author

1.  [Basma Mohamed](https://github.com/basmmohamed) [Basma Mohamed] (https://gitlab.com/basmmohamed) 
