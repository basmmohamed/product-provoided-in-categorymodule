const {
    categoryModel,
    productModel,
    provoiderModel,
    productprovoiderModel,
  } = require("../models/index.js");
  const fs = require("fs");
  exports.getAllProvoider = (req, res) => {
    provoiderModel.find({}).exec((err, allProvoiders) => {
      if (err) return res.json(err);
      res.json({ provoiders: allProvoiders });
    });
  };

  exports.addOneProvoider = (req, res) => {
    const {
      body: { name },
    } = req;
    const provoiderData = new provoiderModel({
      name,
    });
  
    provoiderData.save((err, provoider) => {
      if (err) {
        res.json({ message: err });
  
        return;
      } else {
        // use select for not show id and version
        provoiderModel
          .findById(provoider._id)
          .select("-__v")
          .select("-_id")
          .exec((err, oneProv) => {
            if (err) return res.json(err);
            else res.json({ "Provoider Added": oneProv });
          });
      }
    });
  };
 