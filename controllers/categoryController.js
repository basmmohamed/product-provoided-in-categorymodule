const {
  categoryModel,
  productModel,
  provoiderModel,
  productprovoiderModel,
} = require("../models/index.js");
const fs = require("fs");

exports.getAllProductLowProvoiderPrice = async function db(req, res, next) {
  const resultsArray = [];

  const queryResult = await productprovoiderModel
    .find({})

    .populate({ path: "provoider_id", select: { name: 1, _id: 0 } })
    .populate({
      path: "product_id",
      select: { name: 1, image_uri: 1, _id: 0 },
      populate: {
        path: "category_id",
        select: { name: 1, _id: 0 },
      },
    })
    .where({
      available: true,
    })
    .sort("product_id")
    .sort("price")
    .skip(res.skip) // Always apply 'skip' before 'limit'
    .limit(res.limit)
    .select("-__v")
    .select("-_id")
    .exec((err, allProduct) => {
      if (err) return res.json(err);
      if (allProduct) {
        allProduct.forEach((element) => {
          console.log(
            element.productcategory_id,
            req.params.categoryid,
            element.productcategory_id == req.params.categoryid,
            element.productcategory_id === req.params.categoryid
          );
          if (element.productcategory_id == req.params.categoryid)
            resultsArray.push(element);
        });

        return res.json({
          products: resultsArray,
        });
      }
    });
};

exports.getAllProductInCategory = (req, res) => {
  console.log(req.params.categoryid);

  productModel
    .find({ category_id: req.params.categoryid }, {})
    .populate("category_id", { name: 1 })
    .select("-__v")
    .select("-_id")
    .exec((err, allProduct) => {
      if (err) return res.json(err);
      res.json({ Products: allProduct });
    });
};

exports.featureOneProduct = async (req, res) => {
  let featured;
  try {
    const product = await productModel
      .findOne({ _id: req.params.productid })
      .exec();
    console.log(product);
    featured = product.featured;
    if (req) {
      console.log(featured);
      featured = !featured;
      console.log(featured);
    }
  } catch {
    console.log("err");
  }
  await productModel.findByIdAndUpdate(
    req.params.productid,
    { featured },
    { new: true },
    (err, product) => {
      if (err) return res.send(err);
      res.send(product);
    }
  );
};

exports.addOneCategory = (req, res) => {
  const {
    body: { name, parent_id },
  } = req;
  const categoryData = new categoryModel({
    name,
    parent_id,
  });

  categoryData.save((err, category) => {
    if (err) {
      res.json({ message: err });

      return;
    } else {
      // use select for not show id and version
      categoryModel
        .findById(category._id)
        .select("-__v")
        .select("-_id")
        .exec((err, onecat) => {
          if (err) return res.json(err);
          else res.json({ "Category Added": onecat });
        });
    }
  });
};
exports.getAllCategory = (req, res) => {
  categoryModel.find({}).exec((err, allCategories) => {
    if (err) return res.json(err);
    res.json({ Cats: allCategories });
  });
};
