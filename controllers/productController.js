const {
  categoryModel,
  productModel,
  provoiderModel,
  productprovoiderModel,
} = require("../models/index.js");
const fs = require("fs");

exports.getAllProduct = (req, res) => {
  productModel.find({}).exec((err, allProduct) => {
    if (err) return res.json(err);
    res.json({ products: allProduct });
  });
};

exports.addOneProduct = (req, res) => {
  const {
    body: { name, category_id, image_uri ,featured},
  } = req;
  // const image_ext = req.files[0].originalname.split(".")[1];
  // let image_uri = req.files[0].filename + "." + image_ext;

  // fs.rename(req.files[0].path, process.env.PRODUCT_PICTURES + image_uri, (err) => {
  //   if (err) console.log(err);
  // });

  const productData = new productModel({
    name,
    image_uri,
    category_id,
    featured
  });

  productData.save((err, product) => {
    if (err) {
      res.json({ message: err });
      return;
    } else {
      // use select for not show id and version
      productModel
        .findById(product._id)
        .select("-__v")
        .select("-_id")
        .exec((err, oneprod) => {
          if (err) return res.json(err);
          else res.json({ "Product Added": oneprod });
        });
    }
  });
};
exports.addOneProductByProvoider = (req, res) => {
    const {
      body: { product_id, provoider_id, price, available, productcategory_id },
    } = req;
    const productprovoiderData = new productprovoiderModel({
      product_id,
      provoider_id,
      price,
      available,
      productcategory_id,
    });
  
    productprovoiderData.save((err, productbyprovoider) => {
      if (err) {
        res.json({ message: err });
  
        return;
      } else {
        // use select for not show id and version
        productprovoiderModel
          .findById(productbyprovoider._id)
          .select("-__v")
          .select("-_id")
          .exec((err, oneprodProv) => {
            if (err) return res.json(err);
            else res.json({ "productbyprovoider Added": oneprodProv });
          });
      }
    });
  };

  exports.getAllProductProvoiderPrice = (req, res) => {
    productprovoiderModel.find({}).exec((err, allProduct) => {
      if (err) return res.json(err);
      res.json({ productsWithprovoiders: allProduct });
    });
  };
  